#!/bin/env python3

from scipy import ndimage
import numpy as np
import skimage

def relief_to_concavity(relief, r1, r2):
    """
    Function input is a 2d numpy matrix, which represents a Lidar relief scan.
    Function outputs a same sized 2d numpy matrix that represents concavity of input relief.

    relief: (dtype= 2d numpy array) Lidar relief scan
    r1: (dtype= integer) inner radius of the ring we use for calculating concavity
    r2: (dtype= integer) outer radius of the ring we use for calculating concavity

    Concavity is calculated using convolution via ndimage.convolve.
    """

    # First we create the right sized kernel for convolution
    concavity_kernel = np.zeros((2*r2+1, 2*r2+1))
    # N represents the number of nearby elements we will use for calculating the concavity i.e. number of elements
    # inside the ring.
    N = 0
    for x in range(-r2, r2+1):
        for y in range(-r2, r2+1):
            if (x**2 + y**2) <= r2**2 and (x**2 + y**2) >= r1**2:
                concavity_kernel[y+r2, x+r2] = 1
                N += 1
            else:
                continue

    # The kernel is modified so that concavity in point (i,i) is defined as value of (i,i) minus
    # the average of values in the ring around (i,i).
    concavity_kernel *= -1.0/N
    concavity_kernel[r2, r2] = 1

    concavity = ndimage.convolve(relief, concavity_kernel, mode='nearest')
    return concavity

def sinkhole_indicator_field(concavity, tol=0.5):
    """
    Function takes a 2D matrix that represents the concavity of the relief and returns the same size matrix that
    turns the values of locations not inside a vrtaca to 0 and those inside one to 1.

    field: (dtype= 2D numpy array) field of concavity we wish to clean up

    Function returns a 2D numpy array that gives area of vrtacas the value 1 and everything else the value 0.
    """

    indicator_field = concavity.copy()

    # We calculate a threshold value = delta, which is the standard deviation of elements in the field
    delta = (np.nanstd(concavity))*tol

    for y in range(indicator_field.shape[0]):
        for x in range(indicator_field.shape[1]):
            if np.isnan(concavity[y, x]) or (concavity[y, x] > -delta):
                indicator_field[y, x] = 0
            else:
                indicator_field[y, x] = 1
    return indicator_field.astype(int)

def sinkhole_segmentation(indicator_field):
    # Watershed segmentation
    # https://scikit-image.org/docs/stable/auto_examples/segmentation/plot_watershed.html#sphx-glr-auto-examples-segmentation-plot-watershed-py

    # Takes an array containing only 0s and 1s
    # Changes the 1s so that they equal the distance to the 0 regions
    # Points that are further inside the blobs get higher numbers
    # _edt == exact Euclidian Distance Transform
    distance = ndimage.distance_transform_edt(indicator_field)

    # Finds the local maxima of the distance field,
    # i. e. the points at the center of the blobs.
    #
    # footprint ... limits the search area locally
    # min_distance ... outputs no maxima within min_distance of eachother
    # labels ... in theory segments array into subareas, where a pixel at coords [y, x] is in area labels[y, x]
    #            if labels[y, x] == 0, pixel is interpereted as being in background and ignored.
    #            By setting labels=indicator_field, we ignore all points outside sinkholes (for speed)
    #
    #
    # Could possibly be used on -relief_to_concavity(relief, ...)
    # That does not work ... noise?
    local_maxima_coords = skimage.feature.peak_local_max(distance, footprint=np.ones((10, 10)), min_distance=5, labels=indicator_field)

    # Save the (local) maximal values
    local_maxima_values = np.array([distance[my, mx] for my, mx in local_maxima_coords])

    # Create a mask (bool field here) that points to the maxima
    maxima_location_mask = np.zeros_like(indicator_field, dtype=bool)
    # maxima_location_mask[y, x] == True <=> distance[y, x] is a local maximum
    maxima_location_mask[tuple(local_maxima_coords.T)] = True

    # We give each maximum a label (unique number).
    # This label is stored in a field the same shape as the distance field
    # i. e. if distance[y, x] is a local maximum, is is labeled as sinkhole_label_field[y, x];
    # otherwise sinkhole_label_field[y, x] == 0.
    maxima_label_field, number_of_sinkholes = skimage.morphology.label(maxima_location_mask, return_num=True)

    # Watershead segmentation
    sinkhole_labels = skimage.segmentation.watershed(-distance, maxima_label_field, mask=indicator_field)

    # Returns a list of skimage.measure._regionprops.RegionProperties objects - each has properties pertaining to the given sinkhole such as
    # - area
    # - num_pixels
    # - bbox
    # etc.
    # https://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops
    sinkhole_properties = skimage.measure.regionprops(sinkhole_labels)

    sizes = []
    locations = []
    max_r = []

    for sinkhole in sinkhole_properties:
        # The geometric center of the sinkhole is defined to be the centroid of the pixels contained in the sinkhole
        center = sinkhole.centroid
        # The size of the sinkhole
        size = sinkhole.area
        # The diameter of the sinkhole is defined to be the maximal Feret's diameter,
        # i. e. the length of the longest line that can fit in a shapes convex hull
        maximum_diameter = int(sinkhole.feret_diameter_max)

    return local_maxima_coords, local_maxima_values, sinkhole_labels, sinkhole_properties

_ = """
def sinkhole_segmentation(indicator_field, heights, sigma):
    distance = ndimage.distance_transform_edt(indicator_field)
    h = ndimage.gaussian_filter(heights, sigma)

    # heights needs to be normalized -- i.e. no nan-s
    local_maxima_coords = skimage.feature.peak_local_max(-h, footprint=np.ones((10, 10)), min_distance=5, labels=indicator_field)

    local_maxima_values = np.array([distance[my, mx] for my, mx in local_maxima_coords])

    maxima_location_mask = np.zeros_like(indicator_field, dtype=bool)
    maxima_location_mask[tuple(local_maxima_coords.T)] = True
    
    maxima_label_field, number_of_sinkholes = skimage.morphology.label(maxima_location_mask, return_num=True)

    # Watershead segmentation
    sinkhole_labels = skimage.segmentation.watershed(-h, maxima_label_field, mask=indicator_field)

    # Returns a list of skimage.measure._regionprops.RegionProperties objects - each has properties pertaining to the given sinkhole such as
    # - area
    # - num_pixels
    # - bbox
    # etc.
    # https://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops
    sinkhole_properties = skimage.measure.regionprops(sinkhole_labels)

    sizes = []
    locations = []
    max_r = []

    for sinkhole in sinkhole_properties:
        # The geometric center of the sinkhole is defined to be the centroid of the pixels contained in the sinkhole
        center = sinkhole.centroid
        # The size of the sinkhole
        size = sinkhole.area
        # The diameter of the sinkhole is defined to be the maximal Feret's diameter,
        # i. e. the length of the longest line that can fit in a shapes convex hull
        maximum_diameter = int(sinkhole.feret_diameter_max)

    return local_maxima_coords, local_maxima_values, sinkhole_labels, sinkhole_properties
"""


#def get_sinkhole_edge(sinkhole_labels, number, tol):
#    specific_sinkhole_indicator = (sinkhole_labels == number).astype(int)
#    specific_sinkhole_dist = ndimage.distance_transform_edt(specific_sinkhole_indicator)
#    specific_sinkhole_edge_mask = (specific_sinkhole_dist < tol).astype(int)
#    specific_sinkhole_edge_mask *= (specific_sinkhole_indicator == 1).astype(int)
#    return specific_sinkhole_edge_mask

def get_sinkhole_edge(sinkhole_labels, number, tol_inner, tol_outer=0):
    specific_sinkhole_indicator = (sinkhole_labels == number).astype(int)
    specific_sinkhole_reverse = 1 - specific_sinkhole_indicator
    inner_distance = ndimage.distance_transform_edt(specific_sinkhole_indicator)
    outer_distance = ndimage.distance_transform_edt(specific_sinkhole_reverse)
    return ((outer_distance <= tol_outer) * (inner_distance <= tol_inner)).astype(int)

#def get_sinkhole_edge_weight(sinkhole_labels, number, tol):
#    specific_sinkhole_indicator = (sinkhole_labels == number).astype(int)
#    specific_sinkhole_dist = ndimage.distance_transform_edt(specific_sinkhole_indicator)
#    specific_sinkhole_edge_mask = (specific_sinkhole_dist < tol).astype(int)
#    specific_sinkhole_edge_mask *= (specific_sinkhole_indicator == 1).astype(int)
#    reverse_edge_mask = 1 - specific_sinkhole_edge_mask
#    reverse_distance = ndimage.distance_transform_edt(reverse_edge_mask)
#    interior_max = (reverse_distance * specific_sinkhole_indicator).max()
#    clipped_reverse_distance = (reverse_distance <= interior_max).astype(int) * reverse_distance + (reverse_distance > interior_max).astype(int) * interior_max
#    clipped_distance = interior_max - clipped_reverse_distance
#    return clipped_distance / clipped_distance.max()

def get_sinkhole_edge_weight(sinkhole_labels, number, tol_inner, tol_outer=0):
    specific_sinkhole_indicator = (sinkhole_labels == number).astype(int)
    specific_edge = get_sinkhole_edge(sinkhole_labels, number, tol_inner, tol_outer)
    specific_edge_reverse = 1 - specific_edge
    edge_distance = ndimage.distance_transform_edt(specific_edge_reverse)
    interior_max = (edge_distance * specific_sinkhole_indicator).max()
    if interior_max == 0:
        interior_max = 5
    clipped_reverse_distance = (edge_distance <= interior_max).astype(int) * edge_distance + (edge_distance > interior_max).astype(int) * interior_max
    clipped_distance = interior_max - clipped_reverse_distance
    return clipped_distance / clipped_distance.max()



def fou_curve_implicit(m):
    def implicit_eq(u, x):
        # u ... n x 2 array of (x, y) coordinates
        u_m_x0 = (u.T - x[0:2]).T
        r = np.linalg.norm(u_m_x0, axis=0)
        theta = np.arctan2(u_m_x0[1], u_m_x0[0])
        result = np.zeros_like(r) + x[3]
        for j in range(m):
            omega = j+1
            k = 4 + 2*j
            result +=   x[k] * np.cos(omega*(theta - x[2]))
            result += x[k+1] * np.sin(omega*(theta - x[2]))
        result -= r
        return result
    return implicit_eq

def fou_jac_implicit(m):
    def implicit_eq_diff_curve(u, x, l):
        u_radial = (u - np.array([x[0:2]]).T)
        angle = np.arctan2(u_radial[1], u_radial[0])
        radius = np.linalg.norm(u_radial, axis=0)
        if l == 0: # x[0] = x_0
            curve_derivative = np.zeros(shape=(u.shape[1],))
            for j in range(m):
                k = 4 + 2*j
                omega = j+1
                curve_derivative += -x[k] *omega*np.sin(omega*(angle - x[2]))
                curve_derivative += x[k+1]*omega*np.cos(omega*(angle - x[2]))
            radius_x0 = -u_radial[0]/radius
            angle_x0 = u_radial[1]/np.square(radius)
            return angle_x0 * curve_derivative - radius_x0
        if l == 1: # x[1] = y_0
            curve_derivative = np.zeros(shape=(u.shape[1],))
            for j in range(m):
                k = 4 + 2*j
                omega = j+1
                curve_derivative += -x[k] *omega*np.sin(omega*(angle - x[2]))
                curve_derivative += x[k+1]*omega*np.cos(omega*(angle - x[2]))
            radius_y0 = -u_radial[1]/radius
            angle_y0 = -u_radial[0]/np.square(radius)
            return angle_y0 * curve_derivative - radius_y0
        if l == 2: # x[2] = angle_0
            curve_derivative = np.zeros(shape=(u.shape[1],))
            for j in range(m):
                k = 4 + 2*j
                omega = j+1
                curve_derivative += -x[k] *omega*np.sin(omega*(angle - x[2]))
                curve_derivative += x[k+1]*omega*np.cos(omega*(angle - x[2]))
            return -curve_derivative
        if l == 3: # x[3] = a_0
            return np.zeros(shape=(u.shape[1],)) + 1
        else:
            if l >= 4 + 2*m:
                return x[l] # l must be in range(4+2*m)
            if l % 2 == 0:
                j = (l - 4)//2
                omega = j+1
                return np.cos(omega*(angle - x[2]))
            else:
                j = (l - 5) // 2
                omega = j+1
                return np.sin(omega*(angle - x[2]))
    return implicit_eq_diff_curve

def fou_curve_explicit(m):
    def curve(ts, x):
        radius = np.zeros_like(ts) + x[3]
        for j in range(m):
            k = 4 + 2*j
            omega = j+1
            radius +=   x[k] * np.cos(omega*(ts - x[2]))
            radius += x[k+1] * np.sin(omega*(ts - x[2]))
        points = np.stack((np.cos(ts), np.sin(ts)), axis=0) * radius + np.array([[x[0]], [x[1]]])
        return points
    return curve

def fou_starting_guess(m, center, radius):
    nudge = 0.1
    return [center[0]+nudge, center[1]+nudge, 0, radius] + ([0, 0]*m)

def minimisation_function_maker(curve_impl):
    def fun(x, u, w):
        return w * curve_impl(u, x)
    return fun

def minimisation_jacobi_maker(curve_impl_der, number_of_vars):
    def jac(x, u, w):
        ux, uy = u.shape
        jacobi = np.zeros(shape=(uy, number_of_vars))
        for l in range(number_of_vars):
            jacobi[:, l] = w * curve_impl_der(u, x, l)
        return jacobi
    return jac




def nan_to_min(field):
    min_value = np.nanmin(field)
    copy_of_field = np.copy(field)
    return np.nan_to_num(copy_of_field, nan=min_value)

def get_window(field, center, size, normalize=True):
    cy, cx = center
    sy, sx = field.shape
    min_y = min(min(cy, size), min(sy-cy, size))
    min_x = min(min(cx, size), min(sx-cx, size))
    real_size = min(min_x, min_y)
    window = np.copy(field[(cy-real_size):(cy+real_size), (cx-real_size):(cx+real_size)])
    if normalize:
        window -= np.nanmin(window)
        window /= np.nanmax(window)
    return window, real_size

def coords_to_field(coords, values, field_shape):
    field = np.zeros(shape=field_shape)
    m = values.shape[0]
    for k in range(m):
        y, x = coords[:, k]
        field[y, x] = values[k]
    return field

def sigmoid_weight(h):
    return h/(1 + np.exp(-10*(h - 0.5)))

def make_coords(shape):
    size_y, size_x = shape
    pos_x = np.tile(range(size_x), [1, size_y])
    pos_y = np.reshape(np.tile(range(size_y), [size_x, 1]).T, (1, size_x * size_y))
    pos = np.concatenate((pos_x, pos_y), axis=0) # u
    return pos



def fou_positive(m):
    def implicit_eq(u, x):
        u_radial = (u - np.array([x[0:2]]).T)
        angle = np.arctan2(u_radial[1], u_radial[0])
        radius = np.linalg.norm(u_radial, axis=0)
        result = np.zeros(shape=(u.shape[1],)) + x[3]
        for j in range(m):
            k = 4 + 2*j
            omega = j+1
            result +=   x[k] * (1 + np.cos(omega*(angle - x[2])))
            result += x[k+1] * (1 + np.sin(omega*(angle - x[2])))
        result -= radius
        return result
    return implicit_eq

def fou_positive_explicit(m):
    def curve(ts, x):
        radius = np.zeros_like(ts) + x[3]
        for j in range(m):
            k = 4 + 2*j
            omega = j+1
            radius +=   x[k] * (1 + np.cos(omega*(ts - x[2])))
            radius += x[k+1] * (1 + np.sin(omega*(ts - x[2])))
        points = np.stack((np.cos(ts), np.sin(ts)), axis=0) * radius + np.array([[x[0]], [x[1]]])
        return points
    return curve