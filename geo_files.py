import pandas as pd
import numpy as np
from osgeo import ogr, gdal, osr 
from pyproj import CRS, Transformer # gdal transforms don't work on haswell, and pyproc is great for projection transforms


"""
Examples of use:

# always assume:
  import matplotlib.pyplot as plt

# To load and plot a DEM raster and a vector overlay:
  dem = AscFile(path_to_file__asc)
  shapes = ShpFile(path_to_file__shp, dem)

  plt.figure()
  plt.imshow(dem.dem, cmap='terrain')
  for category in shapes.paths:
    for p in shapes.paths[category]:
      plt.plot(p[0], p[1], 'k-')
  plt.show()

# to plot pure hillshade of a DEM
  # use variable dem from examples above
  plt.imshow(hillshade(dem.dem, -45, 0))

# to plot hillshade on top of DEM color-coded by elevation
  # use variable dem from examples above
  im1 = plt.imshow(dem.dem, cmap='terrain')
  im2 = plt.imshow(hillshade(dem.dem), cmap='Greys', alpha=0.8)

"""

class AscFile:
  """
  Files with extension .asc contain DEM data, registered to some grid
  """
  def __init__(self, path, decimal=',', sep=' '):
    # first 6 lines are metadata
    with open(path) as f:
      lines = [f.readline().strip().replace(decimal, '.') for _ in range(6)]
    # read data lines
    self.dataFrame = pd.read_table(path, decimal=decimal, sep=sep, header=6)
    self.dem = self.dataFrame.to_numpy()
    # 'nodata' value is specified on line 6:
    self.xllcorner = float(lines[2].split(' ')[-1])   # xllcorner, line 3
    self.yllcorner = float(lines[3].split(' ')[-1])   # yllcorner, line 4
    self.cellsize = float(lines[4].split(' ')[-1])   # cellsize, line 5
    self.ysize = self.dem.shape[0]
    self.xsize = self.dem.shape[1]
    self.resolution = self.cellsize
    self.NODATA = float(lines[5].split(' ')[-1])
    self.dem[self.dem == self.NODATA] = np.NAN
    # keep also the legacy variable name "data":
    self.data = self.dem


class TifFile:
  def __init__(self, tif_filename):
    """
    tif_filename: string/pathlib.Path
      filename (with full absolute or relative path) of the geo tif to serve as DEM
    """
    # load the raster
    ds = gdal.Open(tif_filename)
    data = ds.ReadAsArray()
    gt = ds.GetGeoTransform()
    self.gdalGeoTransform = gt
    self.gdalProjection = ds.GetProjection()
    (xres, yres) = (gt[1], gt[5])
    self.resolution = xres
    
    # get the edge coordinates and add half the resolution
    # to get coordinates of pixel centers, assume negative y
    # note: gt[0], gt[3] are coordinates of the edge of the pixel
    xsize = ds.RasterXSize
    ysize = ds.RasterYSize
    xmin_org = gt[0] + xres * 0.5
    xmax_org = gt[0] + xres * (xsize - 0.5)
    ymin_org = gt[3] + yres * (ysize - 0.5)
    ymax_org = gt[3] + yres * 0.5

    if False:
      # TODO: move to a separate function
      # reduce the size / regrid:
      x_org = np.linspace(xmin_org, xmax_org, xsize)
      y_org = np.linspace(ymin_org, ymax_org, ysize)
      f = interpolate.interp2d(x_org, y_org, data, kind='linear')
      x = np.arange(gt[0] + resolution * 0.5, xmax_org, resolution)
      y = np.arange(ymin_org + (resolution - yres) * 0.5, ymax_org, resolution)
      topg = f(x, y)

      # reduce the size of the ice extent vector
      mappedPaths = {}
      for ls in self.paths:
          mappedPaths[ls] = []
          for p in self.paths[ls]:
              p_x = [p_i * abs(xres) / resolution -0.5 for p_i in p[0]]
              p_y = [p_i * abs(yres) / resolution -0.5 for p_i in p[1]]
              mappedPaths[ls].append((p_x, p_y))
      self.paths = mappedPaths
    else:
      self.dem = data
      self.xsize = xsize
      self.ysize = ysize

def resampleDem(instance):
  """
  arguments:
  instance: instance of AscFile or TifFile
    instance of DEM to resize, can be one of the supported raster types defined above (must provide attributes xsize, ysize, resolution, dem)
  """
  if not (hasattr(instance, 'dem') and hasattr(instance, 'ysize') and hasattr(instance, 'xsize') and hasattr(instance, 'resolution')):
    raise RuntimeError(f'instance does not have the required attributes to perform resampling')
  

class ShpFile:
  """
  Files with extension .shp contain vector shapes and the data about the projection they use.
  """
  def __init__(self, path, referenceRaster=None):
    """
    Load a shp file (contiaining vector shapes) for plotting. The shapes will be transformed to conform with the reference raster.
    After being loaded, the shapes can be drawn without coordinate manipulation over the image of reference raster plotted with pyplot.imshow.
    referenceRaster: 
      can be None to avoid transformations
      can be a tuple (rx, ry) and the loaded shape will be transformed to origin (rx, ry), no projection will be applied
      can be a GDAL raster opened by gdal.Open (e.g. a geo tiff file) and shape wil lbe transformed to the raster projection first then moved to the raster origin
    """
    driver = ogr.GetDriverByName('ESRI Shapefile')
    ds = driver.Open(path)
    if (ds == None):
      raise Exception(f'Error: {path} was not found')
    self.layer = ds.GetLayer(0)
    self.numLayers = ds.GetLayerCount()
    self.ds = ds
    self.paths = {}

    if (referenceRaster != None):
      if hasattr(referenceRaster, 'xllcorner') and hasattr(referenceRaster, 'yllcorner'):
        def geometryTransform(geom, pin):
          # x,y are lists of coordinates of points in the geometry
          px = [geom.GetX(j)-referenceRaster.xllcorner for j in range(geom.GetPointCount())]
          py = [referenceRaster.ysize-(geom.GetY(j)-referenceRaster.yllcorner) for j in range(geom.GetPointCount())]
          return (px,py)
      else:
        if hasattr(referenceRaster, 'gdalGeoTransform') and hasattr(referenceRaster, 'gdalProjection'):
          gt = referenceRaster.gdalGeoTransform
          prj = referenceRaster.gdalProjection
        else:
          gt = referenceRaster.GetGeoTransform()
          prj = referenceRaster.GetProjection()
        # geo transform does the following:
        # Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
        # therefore: Xpixel = (Xgeo-GT(0))/GT(1)
        # Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)
        # In case of north up images, the GT(2) and GT(4) coefficients are zero, and the GT(1) is
        # pixel width, and GT(5) is pixel height. The (GT(0),GT(3)) position is the top left corner
        # of the top left pixel of the raster.

        self.pout = CRS.from_wkt(prj)
        
        def geometryTransform(geom, pin):
          if pin is not None:
            transformer = Transformer.from_crs(pin, self.pout)
          else:
            raise RuntimeError('Cannot deal with files without projection info')
          # x,y are lists of coordinates of points in the geometry
          x = [geom.GetX(j) for j in range(geom.GetPointCount())]
          y = [geom.GetY(j) for j in range(geom.GetPointCount())]
          # points from the geometry transformed to the reference taken from the raster
          [t_x, t_y] = transformer.transform(x,y)
          # transformed points translated to pixel indices
          px = [(mx - gt[0]) / gt[1] for mx in t_x]
          py = [(my - gt[3]) / gt[5] for my in t_y]
          return (px,py)
    else:
      def geometryTransform(geom, pin):
        # x,y are lists of coordinates of points in the geometry
        px = [geom.GetX(j) for j in range(geom.GetPointCount())]
        py = [geom.GetY(j) for j in range(geom.GetPointCount())]
        return (px,py)

    for layer in ds:
      dsp = ds.GetLayer(0).GetSpatialRef()
      if dsp is not None:
        pin = dsp.ExportToProj4()
      else:
        pin = None
      
      for feat in layer:
        # for ice extent, the contents of file should include 'clear' and 'unclear' tags for 
        # each segment of line as the second (index=1) field of that line
        if feat.GetFieldCount() > 1:
          category = feat.GetField(1)
        else:
          category = 'unknown'
        if not category in self.paths:
          self.paths[category] = []

        # transform category into linestyle
        if category == 'clear':
          lineStyle = ('-', 2)
        else:
          lineStyle = ('--', 1)

        if not lineStyle in self.paths:
          self.paths[lineStyle] = []
      
        geom = feat.GetGeometryRef()
        rt = None
        if geom.GetGeometryType() == ogr.wkbLineString:
          rt = geometryTransform(geom, pin)
        if geom.GetGeometryType() == ogr.wkbPolygon:
          for i in range(geom.GetGeometryCount()):
            r = geom.GetGeometryRef(i)
            rt = geometryTransform(r, pin)
        if geom.GetGeometryType() == ogr.wkbPolygon25D or geom.GetGeometryType() == ogr.wkbMultiPolygon or geom.GetGeometryType() == ogr.wkbMultiPolygon25D:
          for i in range(geom.GetGeometryCount()):
            r = geom.GetGeometryRef(i)
            rt = geometryTransform(r, pin)
        if rt == None:
          raise TypeError(f'Found an unsuported geometry: {ogr.GeometryTypeToName(geom.GetGeometryType())}.')
        self.paths[category].append(rt)
